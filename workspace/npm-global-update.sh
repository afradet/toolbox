#!/bin/bash

if ! command -v npm &> /dev/null
then
    echo "npm could not be found"
    exit
fi

echo -e "==> Updating NPM "
echo -e "=> Outdated packages :"
npm outdated -g
echo "Update all packages ?"
select yn in "Update" "Do nothing"; do
    case $yn in
        "Update" ) sudo npm update -g; break;;
        "Do nothing" ) echo "No package updated"; break;;
    esac
done

