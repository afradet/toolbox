#!/bin/bash

if [ ! $# -eq 2 ]
  then
    echo "Supply 2 args => 'check-sha.sh file_name sha_to_check'"
    exit
fi

diff <(shasum -a 256 $1 | sed 's/^\(.[a-f0-9]*\) .*$/\1/') <(echo $2)
echo 'Check done'
