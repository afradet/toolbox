#!/usr/bin/python3

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os
import re
import sys

dir_path = os.path.dirname(os.path.realpath(__file__))
directory = os.fsencode(dir_path)


def rename_file_a():

    if len(sys.argv) != 2 or sys.argv[1] != '-r':
        print('\n/!\\/!\\/!\\ Dry run, use -r to execute rename /!\\/!\\/!\\\n')
    else:
        print('\nRenaming...\n')

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.startswith("B_") or filename.startswith("R_"):
            print('old : ' + os.path.join(dir_path, filename))
            new_name = filename
            splits = new_name.split(sep="_", maxsplit=2)
            new_name = splits[1]
            new_name = new_name[0:4] + "-" + new_name[4:6] + "-" + new_name[6:]
            if len(splits) == 3:
                new_name = new_name + ' - ' + splits[2]
            print('new : ' + os.path.join(dir_path, new_name))
            if len(sys.argv) == 2 and sys.argv[1] == '-r':
                os.rename(filename, new_name)
            print('\n')


# Rename format 20210113-Name-rest
def rename_file_b():
    if len(sys.argv) != 2 or sys.argv[1] != '-r':
        print('\n/!\\/!\\/!\\ Dry run, use -r to execute rename /!\\/!\\/!\\\n')
    else:
        print('\nRenaming...\n')

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if re.match(r'^\d{8}-.*', filename):
            print('old : ' + os.path.join(dir_path, filename))
            new_name = filename
            splits = new_name.split(sep="-", maxsplit=2)
            new_name = splits[0]
            new_name = new_name[0:4] + "-" + new_name[4:6] + "-" + new_name[6:]
            if len(splits) == 3:
                new_name = new_name + ' - ' + splits[1] + ' - ' + splits[2]
            print('new : ' + os.path.join(dir_path, new_name))
            if len(sys.argv) == 2 and sys.argv[1] == '-r':
                os.rename(filename, new_name)


if __name__ == '__main__':
    rename_file_b()
