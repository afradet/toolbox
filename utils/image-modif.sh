#!/bin/bash

shopt -s nullglob
shopt -s extglob

optim_jpeg() {
	echo 'optim_jpeg'
	for FILE in *.@(jpeg|jpg); do jpegoptim --size=1000k $FILE;done
	return
}

rotate_180() {
	echo 'rotate_180'
	convert $2 -rotate $1 $3
	return
}

if [[ $# == 0 ]]; then
	optim_jpeg
fi

if [[ $# == 3 ]]; then
    rotate_180 $1 $2 $3
fi 

if [[ $# -ne 0 && $# -ne 3 ]]; then
	echo "Wrong argument, either 0 for 1000k reduction or 'rotation file1 file2' for rotate"
fi

shopt -u nullglob
shopt -u extglob
