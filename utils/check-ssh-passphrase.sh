#!/bin/bash

# avoid issue if no match
shopt -s nullglob

for KEY_FILENAME in ~/.ssh/id_rsa*; do
	[[ $KEY_FILENAME == *.pub ]] && continue

	if [ $(head -n 2 $KEY_FILENAME | grep ENCRYPTED | wc -w) -eq 0 ] ; 
	then 
		echo "KO :: ssh key is NOT encrypted > you may pass the command \"ssh-keygen -p -f $KEY_FILENAME\" to create the passphrase" ;
	else 
		echo "OK :: ~/.ssh/$KEY_FILENAME is encrypted" ;
	fi;
done

shopt -u nullglob
