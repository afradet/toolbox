# Script part for checking

#### Comparison
```sh
[ "$1" = tagada] # posix
[[ $1 == tagada ]] # bash
-eq -lt -le -gt -ge -ne # number
```

#### Shopt options
```sh
shopt -s nullglob # filename patterns which match no files expand to null string rather than themselves
shopt -s extglob # extend pattern matching ()

shopt -u option_name # unset option
```
ExtGlob :
Bash              Regular Expression
?(pattern-list)   (...|...)?
*(pattern-list)   (...|...)*
+(pattern-list)   (...|...)+
@(pattern-list)   (...|...)    [@ not a RE syntax]
!(pattern-list)   "!" used as for negative assertions in RE syntax

#### Select prompt
```sh
while true; do
    read -p "Do you wish to install this program?" yn
    case $yn in
        [Yy]* ) make install; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
```

```sh
echo "Do you wish to install this program?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) make install; break;;
        No ) exit;;
    esac
done
```
#### Check executable present
```sh
if ! command -v MY_PROG &> /dev/null
then
    echo "MY_PROG could not be found"
    exit
fi
```

#### Check shell
```sh
if [[ $(readlink -f /proc/$(ps -o ppid:1= -p $$)/exe) != $(readlink -f "$SHELL") ]]
then 
    echo "Starting the shell..."
    exec "$SHELL"
else
    echo "Not starting a shell."
    exec "$SHELL"
fi
```

#### Associative array
```sh
declare -A animals
animals=( ["moo"]="cow" ["woof"]="dog")
# OR
declare -A animals=( ["moo"]="cow" ["woof"]="dog")

animals['key']='value' # to set value
"${animals[@]}" # to expand the values
"${!animals[@]}" # to expand the keys
```