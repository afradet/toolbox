FROM ubuntu:20.04

RUN apt-get update && \
      apt-get -y install sudo

RUN useradd -m test && echo "test:test" | chpasswd && adduser test sudo

USER test
CMD /bin/bash